import Link from "next/link";
import styles from "./Header.module.css";

export const Header = () => {
  return (
    <div className={styles.Header}>
      <Link href="/todos">
        <a className={styles.Link}>My Todos</a>
      </Link>
      <Link href="/completed">
        <a className={styles.Link}>Completed</a>
      </Link>
      <Link href="/history">
        <a className={styles.Link}>History</a>
      </Link>
      <Link href="/">
        <a className={styles.Link}>Logout</a>
      </Link>
    </div>
  );
};
