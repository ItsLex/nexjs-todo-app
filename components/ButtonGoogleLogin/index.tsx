import React from "react";
import { loginWithGoogle } from "../../auth";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faGoogle } from "@fortawesome/free-brands-svg-icons";

export const LoginGoogle = () => {
  return (
    <div>
      <button onClick={loginWithGoogle}>
        <FontAwesomeIcon icon={faGoogle} size="2x" color="red" />
        Sign in with Google
      </button>
    </div>
  );
};
