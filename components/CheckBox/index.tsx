import React from "react";
import { useState } from "react";

interface Props {
  isChecked: boolean;
  onClick: () => void;
  className?: string;
}

export const Checkbox = ({ isChecked, onClick, className }: Props) => {
  return (
    <div>
      <input
        type="checkbox"
        checked={isChecked}
        onClick={onClick}
        className={className}
      />
    </div>
  );
};
