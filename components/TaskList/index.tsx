import React from "react";
import { Task } from "../../types";
import { useState } from "react";
import { useEffect } from "react";
import styles from "./TaskList.module.css";
import { Checkbox } from "../CheckBox";

interface Props {
  tasks: Task[];
}

export const TaskList = ({ tasks }: Props) => {
  const [tasksList, setTaskList] = useState([]);

  useEffect(() => {
    const newTaskList = renderTasks();
    setTaskList(newTaskList);
  }, [tasks, setTaskList]);

  const renderTasks = () => {
    return tasks.map((task: Task, index) => {
      return (
        <div>
          <table className={styles.Table}>
            <tr>
              <td className={styles.TableData}>
                <text className={styles.Task}>{task.name}</text>
              </td>
              <td className={styles.TableData}>
                <Checkbox
                  isChecked={task.completed}
                  onClick={() => console.log("Click!")}
                />
              </td>
            </tr>
          </table>
        </div>
      );
    });
  };

  return <div>{tasksList}</div>;
};
