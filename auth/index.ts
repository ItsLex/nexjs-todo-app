import * as firebase from 'firebase'
export const loginWithGoogle = async () => {

    const provider = new firebase.auth.GoogleAuthProvider();

    try {

        const {user, credential} = await firebase.auth().signInWithPopup(provider)
    } catch (err) {
        console.log("Error ", err)
    }
}

    