import * as firebase from 'firebase'
//import firebase from "firebase/app";
//import "firebase/auth";
import "firebase/firestore";

// const dotenv = require('dotenv').config();
// const dev = process.env.NODE_ENV !== 'production';

export const loadFirebase = () => {
  const firebaseConfigStaging = {
      apiKey: "AIzaSyCVH0k76yiHEAW_qZpMwXOpYNRGq3TtxNQ",
      authDomain: "next-js-todo-staging.firebaseapp.com",
      databaseURL: "https://next-js-todo-staging.firebaseio.com",
      projectId: "next-js-todo-staging",
      storageBucket: "next-js-todo-staging.appspot.com",
      messagingSenderId: "285658013989",
      appId: "1:285658013989:web:a604b48bb58454bd81bacc"  
  };
  // Initialize Firebase
  if (!firebase.apps.length) {

    firebase.initializeApp(firebaseConfigStaging);
    console.log("Connected to the database")
  }
 
  return firebase.firestore()
}



