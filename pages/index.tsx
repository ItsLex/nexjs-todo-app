import { Header, LoginGoogle, TaskList } from "../components";
import { loadFirebase } from "../firebase";
import { useEffect } from "react";
import { useState } from "react";
import Button from "@material-ui/core/Button";

const db = loadFirebase();

const Index = () => {
  const [tasks, setTasks] = useState([]);
  const getTasks = () => {
    db.collection("todos")
      //.where("completed", "==", false)
      .get()
      .then((snapShot) => {
        if (snapShot.empty) {
          console.log("No data to be found");
          return;
        }
        let dataList = [];
        snapShot.forEach((doc) => {
          console.log(doc.id, ".....", doc.data());
          const data = doc.data();
          dataList = [...dataList, data];
        });
        if (JSON.stringify(tasks) !== JSON.stringify(dataList)) {
          setTasks(dataList);
        }
      });
  };

  useEffect(() => {
    getTasks();
  }, [tasks, setTasks]);
  console.log("Task ", tasks);
  return (
    <div>
      <Header />
      {/* <LoginGoogle /> */}
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          //height: window.innerHeight,
          // backgroundColor: "grey",
        }}
      >
        <TaskList tasks={tasks}></TaskList>
      </div>
    </div>
  );
};

export default Index;
