import { Header } from "../components";

const Home = () => {
  return (
    <div>
      <Header />
      <h1> Todo</h1>
    </div>
  );
};

export default Home;
